expectValue = function (minlim, maxlim, accuracy)
	local retRes, p = 0, accuracy/(maxlim-minlim)
	for ix = minlim, maxlim, accuracy do
		retRes = retRes + accuracy*p
	end
	return retRes
end

varianceValue = function (mu, minlim, maxlim, accuracy)
	local retRes, p = 0, accuracy/(maxlim-minlim)
	mu = mu or pubTable.expectValue(minlim, maxlim, accuracy)
	for ix = minlim, maxlim, accuracy do
		retRes = retRes + math.pow(ix-mu,2)*p
	end
	return retRes
end

factorial = function (x)
	if x > 0 then
		retValue = x*pubTable.factorial(x-1)
	elseif x == 0 then
		retValue = 1
	end
	return retValue
end
------------------------------------------------------------------------
fibonachiGen = function(arr, a, b)
	local retValue = 0
	local newNum = #arr + 1
	if arr[newNum-a] >= arr[newNum-b] then
		retValue = arr[newNum-a] - arr[newNum-b]
	else
		retValue = arr[newNum-a] - arr[newNum-b] + 1
	end
	return retValue
end

paretoRandomNum = function(x, shape, x0)
	x0 = x0 or 0.1
	shape = shape or 3
	scale = scale or 1
	local retValue = 0
	retValue =  x0*math.pow(x, -1/shape)
	return retValue
end
-- if scale == 1 - Exponential distribution
-- if scale >= 1 - Erlang distribution
-- if scale == n/2 & shape = 1/2 - Chi-squared distribution
gammaRandomNum = function(x, shape, scale)
	shape = shape or 1
	scale = scale or 1
	mu = mu or 0
	local retValue = 0
	
	local S1, S2 = 1, 1 
	local randValue1, randValue2, randValue3 = 0, 0, 0
	
	while (S1+S2) > 1 do
		randValue1 = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue1)
		randValue2 = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue2)	
		
		S1 = math.pow(randValue1, 1/shape)
		S2 = math.pow(randValue2, 1/(1-shape))
	end
	
	retValue = (S1*math.log(x))/(scale*(S1+S2))
	
	return retValue
end

expRandomNum = function(x, shape)
	shape = shape or 1
	local retValue = 0
	
	retValue = -math.log(x)/shape
	
	return retValue
end

erlangRandomNum = function(shape, m)
	shape = shape or 1 
	m = m or 2
	local retValue = 1
	for ix = 1, m, 1 do
		local randValue = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue)
		retValue = retValue*randValue
	end
	retValue = -math.log(retValue)/shape
	
	if retValue > 10 or retValue < 10 then
		retValue = 1
	end
	return retValue
end

betaRandomNum = function(x, shapeAlpha, shapeBeta, Ga, Gb, Gab)
	shapeAlpha = shapeAlpha or 2
	shapeBeta = shapeBeta or 2
	Ga = Ga or pubTable.gammaFunction(shapeAlpha)
	Gb = Gb or pubTable.gammaFunction(shapeBeta)
	Gab = Gab or pubTable.gammaFunction(shapeAlpha + shapeBeta)
	local retValue = 0
	
	if x>0 and x<1 then
		retValue = math.pow(1/shapeAlpha-(x*Ga*Gb)/Gab, shapeAlpha+shapeBeta-2)
	end
	
	return retValue
end
