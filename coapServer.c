#include <stdio.h>
#include <libnyoci/libnyoci.h>

static nyoci_status_t
request_handler(void* context)
{
	printf("Got a request!\n");

	if(nyoci_inbound_get_code() != COAP_METHOD_GET) {
		return NYOCI_STATUS_NOT_IMPLEMENTED;
	}

	nyoci_outbound_begin_response(COAP_RESULT_205_CONTENT);

	nyoci_outbound_add_option_uint(
		COAP_OPTION_CONTENT_TYPE,
		COAP_CONTENT_TYPE_TEXT_PLAIN
	);

	//nyoci_outbound_append_content("", NYOCI_CSTR_LEN);

	return nyoci_outbound_send();
}

int
main(void)
{
	nyoci_t instance;

	//NYOCI_LIBRARY_VERSION_CHECK();

	instance = nyoci_create();

	if (!instance) {
		perror("Unable to create LibNyoci instance");
		exit(EXIT_FAILURE);
	}

	nyoci_plat_bind_to_port(instance, NYOCI_SESSION_TYPE_UDP, COAP_DEFAULT_PORT);

	printf("Listening on port %d\n",nyoci_plat_get_port(instance));

	nyoci_set_default_request_handler(instance, &request_handler, NULL);

	while (1) {
		nyoci_plat_wait(instance, CMS_DISTANT_FUTURE);
		nyoci_plat_process(instance);
	}

	nyoci_release(instance);

	return EXIT_SUCCESS;
}
