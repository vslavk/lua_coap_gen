#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>
#include <limits.h>

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <math.h>
#include <time.h>
#include <sys/timeb.h>

#include <lua.h>                               
#include <lauxlib.h> 
#include <lualib.h> 

#include <coap/coap.h>

#define START_RAND_ARR_SIZE 55

static coap_tid_t id;

/*static int c_sendMessage(lua_State *L);

static const luaL_reg R[] = {
    {"sendMessage", c_sendMessage}, 
    {NULL, NULL}
};*/

coap_pdu_t * make_pdu( unsigned int value ) 
{
	coap_pdu_t *pdu;
	unsigned char enc;
	static unsigned char buf[20];
	int len, ls;

	if ( ! ( pdu = coap_new_pdu() ) )
		return NULL;

	pdu->hdr->type = COAP_MESSAGE_NON;
	pdu->hdr->code = COAP_REQUEST_POST;
	pdu->hdr->id = htons(id++);

	enc = COAP_PSEUDOFP_ENCODE_8_4_DOWN(value,ls);
	coap_add_data( pdu, 1, &enc);

	len = sprintf((char *)buf, "%u", COAP_PSEUDOFP_DECODE_8_4(enc));
	 
	if ( len > 0 ) 
	{
		coap_add_data( pdu, len, buf );
	}

	return pdu;
}

coap_context_t * get_context(const char *node, const char *port) 
{
	coap_context_t *ctx = NULL;  
	int s;
	struct addrinfo hints;
	struct addrinfo *result, *rp;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Coap uses UDP */
	hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST | AI_NUMERICSERV | AI_ALL;
  
	s = getaddrinfo(node, port, &hints, &result);
	if ( s != 0 ) 
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		return NULL;
	}

	/*iterate through results until success */
	for (rp = result; rp != NULL; rp = rp->ai_next) 
	{
		ctx = coap_new_context(rp->ai_addr, rp->ai_addrlen);
		if (ctx) 
		{
			/* TODO: output address:port for successful binding */
			goto finish;
		}
	}
  
	fprintf(stderr, "no context available for interface '%s'\n", node);

	finish:
	freeaddrinfo(result);
	return ctx;
}

void bail(lua_State *L, char *_msg)
{
	fprintf(stderr, "\nFATAL ERROR:\n  %s: %s\n\n", _msg, lua_tostring(L, -1));
	exit(1);
}

static uint8_t sendMessage(const char* _host, uint16_t _port, const unsigned char* _data)
{
	uint8_t result = EXIT_FAILURE;
	coap_context_t  *ctx;
	struct timeval tv;
	coap_pdu_t  *pdu;
	struct sockaddr_in dst;
	int hops = 16;

	ctx = get_context("::", NULL);
	if ( !ctx )
		return -1;

	id = rand() & INT_MAX;

	memset(&dst, 0, sizeof(struct sockaddr_in ));
	dst.sin_family = AF_INET;
	inet_pton( AF_INET, _host, &dst.sin_addr );
	dst.sin_port = _port ? _port : htons( COAP_DEFAULT_PORT );

	if ( IN_IS_ADDR_MULTICAST(&dst.sin_addr) )
	{
		if ( setsockopt( ctx->sockfd, IPPROTO_IP, IP_UNICAST_IF, (char *)&hops, sizeof(hops) ) < 0 )
			perror("setsockopt: IPV6_MULTICAST_HOPS");
	}

	while ( 1 ) 
	{
		if (! (pdu = make_pdu( rand() & 0xfff ) ) )
			return -1;

		coap_send( ctx, (struct sockaddr *)&dst, sizeof(dst), pdu );
		coap_delete_pdu(pdu);

		tv.tv_sec = 5; tv.tv_usec = 0;

		select( 0, 0, 0, 0, &tv );
	}

	coap_free_context( ctx );

	result = EXIT_SUCCESS;
	return result;
}

double* createRandomArray(uint16_t _maxArraySize)
{
	double* retArr = (double*)malloc((_maxArraySize)*sizeof(double));
	FILE *randFile = fopen("/dev/random", "r");
	
	char bufStr[_maxArraySize+1];
	
	fgets(bufStr, _maxArraySize+1, randFile);
	
	double max = 0;
	for(int ix = 0; ix < _maxArraySize; ++ix)
	{	
		retArr[ix] =  (double)(abs(bufStr[ix]));//0xFF;
		if(max < retArr[ix])
		{
			max = retArr[ix];
		}
	}
	
	for(int ix = 0; ix < _maxArraySize; ++ix)
	{	
		retArr[ix] = retArr[ix]/(max+1);
		//fprintf(stdout, "%lf ", retArr[ix]); 
	}
	
	fclose(randFile);
	return retArr;
}

char* readSysData(const char* _dataName, uint16_t _sensorsCount, const char* _uriFirstPart, const char* _uriSecondPart, uint32_t _divider)
{
	char* retStr = (char*)malloc(2*sizeof(char));
	strcpy(retStr, "");
	
	for(int ix = 0; ix < _sensorsCount; ++ix)
	{
		char sysdata[10];
		FILE *sysFile;
		
		uint32_t len = strlen(_uriFirstPart) + strlen(_uriSecondPart) + 2;
		char* buf = (char*)malloc(len*sizeof(char));
		
		sprintf(buf, "%s%d%s", _uriFirstPart, ix, _uriSecondPart);

		sysFile = fopen(buf, "r");
		fgets(sysdata, 10, sysFile);
		sprintf(sysdata, "%.2lf", strtod(sysdata, NULL)/_divider);
		fclose(sysFile);
		
		buf = (char*)realloc(buf, sizeof(retStr)/sizeof(char));
		strcpy(buf, retStr);
		
		len = strlen(buf) + strlen(_dataName) + strlen(sysdata) + 6;
		retStr = (char*)realloc(retStr, len*sizeof(char));
		
		if(ix < _sensorsCount-1)
		{
			sprintf(retStr, "%s%s[%d]=%s&", retStr, _dataName, ix, sysdata);
		} 
		else 
		{
			sprintf(retStr, "%s%s[%d]=%s", retStr, _dataName, ix, sysdata);
		}
	}
	
	return retStr;
}

uint8_t addToRandArray(double* _randomArr, double _newNum)
{
	uint16_t arrSize = sizeof(_randomArr)/sizeof(double);
	double buf[arrSize];
	
	for(int ix = 0; ix < arrSize; ++ix)
	{
		buf[ix] = _randomArr[ix];
	}
	
	_randomArr = (double*)realloc(_randomArr, (arrSize+1)/sizeof(char));
	
	for(int ix = 0; ix < arrSize; ++ix)
	{
		_randomArr[ix] = buf[ix];
	}
	
	_randomArr[arrSize] = _newNum;
	
	return 1;
}

uint8_t main(void) 
{
	unsigned char* data = (unsigned char*)malloc(2*sizeof(char));
	double* randomArr = createRandomArray(START_RAND_ARR_SIZE);
	uint32_t iterationCount = 0;
	
	//clock_t start, stop;
	//double clockEstimated;
	//start = clock();
	//stop = clock();
	//clockEstimated = ( (double)stop - (double)start )/1e-6/CLOCKS_PER_SEC;
	
	//fprintf(stdout, "%lf\n", clockEstimated);
	
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	if (luaL_loadfile(L, "senderFunctions.lua"))
		bail(L, "luaL_loadfile() failed");
	if (lua_pcall(L, 0, 0, 0))
		bail(L, "lua_pcall() failed");

	while(iterationCount < 1000)
	{
		const char* tempStr = readSysData("temp", 2, "/sys/class/thermal/thermal_zone", "/temp", 1000);
		const char* cpufStr = readSysData("cpufr", 3, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000);
		
		uint32_t dataLen = (strlen(tempStr) + strlen(cpufStr) + 8);
		data = (unsigned char*)realloc(data, dataLen * sizeof(unsigned char));
		sprintf(data, "query?%s&%s", tempStr, cpufStr);
		
		sendMessage("localhost", 5683, (const unsigned char*)data);
		fprintf(stdout, "\nsended %s\n", data);
		
		lua_getglobal(L, "fibonachiGen");
		lua_newtable(L);
		for(int ix = 0; ix < START_RAND_ARR_SIZE; ++ix)
		{
			lua_pushnumber(L, ix);
			lua_pushnumber(L, randomArr[ix]);
			lua_settable(L, -3);
		}
		lua_pushnumber(L, 55);
		lua_pushnumber(L, 24);
		
		if (lua_pcall(L, 3, 1, 0))
			bail(L, "lua_pcall() failed"); 

		lua_pushnil(L);
		
		double randomNumber = luaL_checknumber(L, -1);
		
		addToRandArray(randomArr, randomNumber);
		
		lua_getglobal(L, "paretoRandomNum");
		lua_pushnumber(L, randomNumber);
		lua_pushnumber(L, 1);
		if (lua_pcall(L, 3, 1, 0))
			bail(L, "lua_pcall() failed");
		randomNumber = luaL_checknumber(L, -1)*1000000;
		usleep (randomNumber);
		
		++iterationCount;
		
		free((void*)tempStr);
		free((void*)cpufStr);
	}
	
	lua_close(L);
	
	free((void*)randomArr);
	free((void*)data);
	
	return 1; 
}
