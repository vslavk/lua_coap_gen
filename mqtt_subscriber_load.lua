--local mqtt = require('paho.mqtt')
local socket = require('socket')
local sqlite = require('luasql.sqlite3')
math.randomseed(os.time())

subTable = {} 

subTable.ip = '127.0.0.1'
subTable.port = 8888
subTable.topic = 'topic/test'
subTable.name = 'mqtt-lua-subscriber'
subTable.stopLabel = true

subTable.reciveData = {"temp", "cpufr"}
subTable.sortData = {}

subTable.findData = function(payload, findStr)
	local retarr, findpos = {}, 0
	while findpos ~= nil do
		findpos = string.find(payload, findStr, findpos)
		if findpos ~= nil then
			local endpos = 0
			endpos = string.find(payload, "&", findpos)
			if endpos ~= nil then
				table.insert(retarr, string.sub(payload, findpos+findStr:len()+2, endpos-1))
			else
				table.insert(retarr, string.sub(payload, findpos+findStr:len()+2))
			end
			findpos = endpos
		end
	end
	return retarr
end
--[[
subTable.callback = function(topic, payload)
	--print(string.format('Hey, %s just received: %s.', topic, payload))
	local epochtime = os.time()
	if string.find(payload, 'data') then
		for ix = 1, #subTable.reciveData, 1 do
			local arr = subTable.findData(payload, subTable.reciveData[ix])
			if #arr > 0 then
				table.insert(subTable.sortData, arr)
			end
		end
		local env  = sqlite.sqlite3()
		local conn = env:connect("cpu_temp.sqlite")
		local row1, row2 = 
		"INSERT INTO sysdata_cpu (cpu1, cpu2, cpu3, cpu4) VALUES('" .. subTable.sortData[2][1] .. "','" 
																	.. subTable.sortData[2][2] .. "','"
																	.. subTable.sortData[2][3] .. "','"
																	.. subTable.sortData[2][4] .. "');",
		"INSERT INTO sysdata_temp (temp1, temp2, temp3) VALUES('" 	.. subTable.sortData[1][1] .. "','" 
																	.. subTable.sortData[1][2] .. "','"  
																	.. subTable.sortData[1][3] .. "');"
		
		conn:execute(row1)
		conn:execute(row2)
		conn:close()
		env:close()
		
	elseif string.find(payload, 'configure') then
		io.write("-------\n")
		local firstpos, secondpos = string.find(payload, "="), string.find(payload, "&")
		if string.sub(payload, firstpos+1, secondpos) == "temp" then
			table.insert(subTable.reciveData, "temp")
		end
		if string.sub(payload, secondpos+1) == "cpufr" then
			table.insert(subTable.reciveData, "cpufr")
		end
	elseif string.find(payload, 'stop') then
		subTable.stopLabel = false
	else
	
	end
	subTable.sortData = {}
end
]]
subTable.proceedData = function(payload)
	local epochtime = os.time()
	if string.find(payload, 'data') then
		for ix = 1, #subTable.reciveData, 1 do
			local arr = subTable.findData(payload, subTable.reciveData[ix])
			if #arr > 0 then
				table.insert(subTable.sortData, arr)
			end
		end
		local env  = sqlite.sqlite3()
		local conn = env:connect("cpu_temp.sqlite")
		local row1, row2 = 
		"INSERT INTO sysdata_cpu (cpu1, cpu2, cpu3, cpu4) VALUES('" .. subTable.sortData[2][1] .. "','" 
																	.. subTable.sortData[2][2] .. "','"
																	.. subTable.sortData[2][3] .. "','"
																	.. subTable.sortData[2][4] .. "');",
		"INSERT INTO sysdata_temp (temp1, temp2, temp3) VALUES('" 	.. subTable.sortData[1][1] .. "','" 
																	.. subTable.sortData[1][2] .. "','"  
																	.. subTable.sortData[1][3] .. "');"
		
		conn:execute(row1)
		conn:execute(row2)
		conn:close()
		env:close()
		
	elseif string.find(payload, 'configure') then
		io.write("-------\n")
		local firstpos, secondpos = string.find(payload, "="), string.find(payload, "&")
		if string.sub(payload, firstpos+1, secondpos) == "temp" then
			table.insert(subTable.reciveData, "temp")
		end
		if string.sub(payload, secondpos+1) == "cpufr" then
			table.insert(subTable.reciveData, "cpufr")
		end
	elseif string.find(payload, 'stop') then
		subTable.stopLabel = false
	else
	
	end
	subTable.sortData = {}
end

subTable.body = function()
	--[[
	local client = mqtt.client.create(subTable.ip, subTable.port, subTable.callback)
	client:connect(subTable.name)
	client:subscribe({subTable.topic})
	]]
	local udp = assert(socket.udp())
	assert(udp:setsockname(subTable.ip, subTable.port))
	--assert(udp:settimeout(1))
	local dgram, ip, port = '', '', 0--udp:getsockname()
	
	local delay = math.pow(10, -3)
	while subTable.stopLabel do
		--client:handler()	
		dgram, ip, port = udp:receive()
		print(dgram)
		socket.sleep(delay)
	end
	--[[
	client:unsubscribe({subTable.topic})
	client:disconnect()
	client:destroy()
	]]
end

subTable.body()
