#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <math.h>
#include <time.h>
#include <sys/timeb.h>

#include <lua.h>                               
#include <lauxlib.h> 
#include <lualib.h> 

#include <coap2/coap.h>
#include <coap2/net.h>

#define START_RAND_ARR_SIZE 55

/*static int c_sendMessage(lua_State *L);

static const luaL_reg R[] = {
    {"sendMessage", c_sendMessage}, 
    {NULL, NULL}
};*/

uint16_t resolve_address(const char *_host, uint16_t _port, coap_address_t *_dst) 
{

	struct addrinfo *res, *ainfo;
	struct addrinfo hints;
	uint16_t error, len=-1;

	memset(&hints, 0, sizeof(hints));
	memset(_dst, 0, sizeof(*_dst));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_UNSPEC;

	char service[5];
	//sprintf(service, "%d", _port);
	
	error = getaddrinfo(_host, service, &hints, &res);

	if (error != 0) 
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return error;
	}

	for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next) 
	{
		switch (ainfo->ai_family) 
		{
			case AF_INET6:
			case AF_INET:
				len = _dst->size = ainfo->ai_addrlen;
				memcpy(&_dst->addr.sin6, ainfo->ai_addr, _dst->size);
				goto finish;
			default:
			;
		}
	}

	finish:
	freeaddrinfo(res);
	return len;
}

void bail(lua_State *L, char *_msg)
{
	fprintf(stderr, "\nFATAL ERROR:\n  %s: %s\n\n", _msg, lua_tostring(L, -1));
	exit(1);
}

void onReciveMessage(const coap_endpoint_t *_ep, const coap_address_t *_add, 
					 coap_pdu_t *_sender, coap_pdu_t *_received, const coap_tid_t _id) 
{
	coap_show_pdu(LOG_INFO, _received);
}

static uint8_t sendMessage(const char* _host, uint16_t _port, const unsigned char* _data)
{
	coap_context_t  *ctx = NULL;
	coap_session_t *session = NULL;
	coap_address_t dst;
	coap_pdu_t *pdu = NULL;
	uint8_t result = EXIT_FAILURE;;

	fprintf(stdout, "1\n");

	coap_startup();

	fprintf(stdout, "2\n");

	//char* port = (char*)malloc(5*sizeof(char));
	//sprintf(port, "%d", _port);
	if (resolve_address(_host, _port, &dst) < 0)//"coap.me", "5683", &dst) < 0) 
	{
		coap_log(LOG_CRIT, "failed to resolve address\n");
		goto finish;
	}

	ctx = coap_new_context(NULL);

	fprintf(stdout, "3\n");

	if (!ctx || !(session = coap_new_client_session(ctx, NULL, &dst, COAP_PROTO_UDP))) 
    {
		coap_log(LOG_EMERG, "cannot create client session\n");
		goto finish;
	}

	coap_register_response_handler(ctx, (coap_response_handler_t) onReciveMessage);

	fprintf(stdout, "4\n");

	pdu = coap_pdu_init(COAP_MESSAGE_CON,
                      COAP_REQUEST_GET,
                      0 /* message id */,
                      coap_session_max_pdu_size(session));
	if (!pdu) 
	{
		coap_log( LOG_EMERG, "cannot create PDU\n" );
		goto finish;
	}

	fprintf(stdout, "5\n");

	uint16_t dataLength = sizeof(_data)/sizeof(const unsigned char);
	coap_add_option(pdu, COAP_OPTION_URI_PATH, dataLength,_data);

	fprintf(stdout, "6\n");

	coap_send(session, pdu);

	fprintf(stdout, "7\n");

	coap_run_once(ctx, 0);

	fprintf(stdout, "8\n");

	result = EXIT_SUCCESS;
	
	finish:

	coap_session_release(session);
	coap_free_context(ctx);
	coap_cleanup();

	return result;
}

double* createRandomArray(uint16_t _maxArraySize)
{
	double* retArr = (double*)malloc((_maxArraySize)*sizeof(double));
	FILE *randFile = fopen("/dev/random", "r");
	
	char bufStr[_maxArraySize+1];
	
	fgets(bufStr, _maxArraySize+1, randFile);
	
	double max = 0;
	for(int ix = 0; ix < _maxArraySize; ++ix)
	{	
		retArr[ix] =  (double)(abs(bufStr[ix]));//0xFF;
		if(max < retArr[ix])
		{
			max = retArr[ix];
		}
	}
	
	for(int ix = 0; ix < _maxArraySize; ++ix)
	{	
		retArr[ix] = retArr[ix]/(max+1);
		//fprintf(stdout, "%lf ", retArr[ix]); 
	}
	
	fclose(randFile);
	return retArr;
}

char* readSysData(const char* _dataName, uint16_t _sensorsCount, const char* _uriFirstPart, const char* _uriSecondPart, uint32_t _divider)
{
	char* retStr = (char*)malloc(2*sizeof(char));
	strcpy(retStr, "");
	
	for(int ix = 0; ix < _sensorsCount; ++ix)
	{
		char sysdata[10];
		FILE *sysFile;
		
		uint32_t len = strlen(_uriFirstPart) + strlen(_uriSecondPart) + 2;
		char* buf = (char*)malloc(len*sizeof(char));
		
		sprintf(buf, "%s%d%s", _uriFirstPart, ix, _uriSecondPart);

		sysFile = fopen(buf, "r");
		fgets(sysdata, 10, sysFile);
		sprintf(sysdata, "%.2lf", strtod(sysdata, NULL)/_divider);
		fclose(sysFile);
		
		buf = (char*)realloc(buf, sizeof(retStr)/sizeof(char));
		strcpy(buf, retStr);
		
		len = strlen(buf) + strlen(_dataName) + strlen(sysdata) + 6;
		retStr = (char*)realloc(retStr, len*sizeof(char));
		
		if(ix < _sensorsCount-1)
		{
			sprintf(retStr, "%s%s[%d]=%s&", retStr, _dataName, ix, sysdata);
		} 
		else 
		{
			sprintf(retStr, "%s%s[%d]=%s", retStr, _dataName, ix, sysdata);
		}
	}
	
	return retStr;
}

uint8_t addToRandArray(double* _randomArr, double _newNum)
{
	uint16_t arrSize = sizeof(_randomArr)/sizeof(double);
	double buf[arrSize];
	
	for(int ix = 0; ix < arrSize; ++ix)
	{
		buf[ix] = _randomArr[ix];
	}
	
	_randomArr = (double*)realloc(_randomArr, (arrSize+1)/sizeof(char));
	
	for(int ix = 0; ix < arrSize; ++ix)
	{
		_randomArr[ix] = buf[ix];
	}
	
	_randomArr[arrSize] = _newNum;
	
	return 1;
}

uint8_t main(void) 
{
	unsigned char* data = (unsigned char*)malloc(2*sizeof(char));
	double* randomArr = createRandomArray(START_RAND_ARR_SIZE);
	uint32_t iterationCount = 0;
	
	//clock_t start, stop;
	//double clockEstimated;
	//start = clock();
	//stop = clock();
	//clockEstimated = ( (double)stop - (double)start )/1e-6/CLOCKS_PER_SEC;
	
	//fprintf(stdout, "%lf\n", clockEstimated);
	
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	if (luaL_loadfile(L, "senderFunctions.lua"))
		bail(L, "luaL_loadfile() failed");
	if (lua_pcall(L, 0, 0, 0))
		bail(L, "lua_pcall() failed");

	while(iterationCount < 1000)
	{
		const char* tempStr = readSysData("temp", 2, "/sys/class/thermal/thermal_zone", "/temp", 1000);
		const char* cpufStr = readSysData("cpufr", 3, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000);
		
		uint32_t dataLen = (strlen(tempStr) + strlen(cpufStr) + 8);
		data = (unsigned char*)realloc(data, dataLen * sizeof(unsigned char));
		sprintf(data, "query?%s&%s", tempStr, cpufStr);
		
		sendMessage("localhost", 5683, (const unsigned char*)data);
		fprintf(stdout, "\nsended %s\n", data);
		
		lua_getglobal(L, "fibonachiGen");
		lua_newtable(L);
		for(int ix = 0; ix < START_RAND_ARR_SIZE; ++ix)
		{
			lua_pushnumber(L, ix);
			lua_pushnumber(L, randomArr[ix]);
			lua_settable(L, -3);
		}
		lua_pushnumber(L, 55);
		lua_pushnumber(L, 24);
		
		if (lua_pcall(L, 3, 1, 0))
			bail(L, "lua_pcall() failed"); 

		lua_pushnil(L);
		
		double randomNumber = luaL_checknumber(L, -1);
		
		addToRandArray(randomArr, randomNumber);
		
		lua_getglobal(L, "paretoRandomNum");
		lua_pushnumber(L, randomNumber);
		lua_pushnumber(L, 1);
		if (lua_pcall(L, 3, 1, 0))
			bail(L, "lua_pcall() failed");
		randomNumber = luaL_checknumber(L, -1)*1000000;
		usleep (randomNumber);
		
		++iterationCount;
		
		free((void*)tempStr);
		free((void*)cpufStr);
	}
	
	lua_close(L);
	
	free((void*)randomArr);
	free((void*)data);
	
	return 1; 
}
