--local mqtt = require('paho.mqtt')
local ffi = require("ffi")
local socket = require('socket')
--local torch = require('torch')
--local randomkit = require('randomkit')
--local coapSender =  ffi.load('libcoapSender')

ffi.cdef([[int sendMessage(const char* host, uint16_t port, const unsigned char* data);]])

--local coapSender2 =  require('libcoapSender2')
--ffi.cdef[[int sendString (const char* url, const char* data);]]
--local libnyociClient = ffi.load('libnyociClient')

math.randomseed(os.time())

local pubTable = {} 

pubTable.ip = '127.0.0.1'
pubTable.port = 8888
pubTable.topic = 'topic/test'
pubTable.name = 'mqtt-lua-publisher'
pubTable.stopLabel = true
pubTable.mesSize = 128

pubTable.randomArr = {}

--print(c_str)

------------------------------------------------------------------------
-----------------------Math function------------------------------------
------------------------------------------------------------------------
pubTable.expectValueForArray = function (array, p)
	local retRes= 0
	for ix = 1, #array, 1 do
		retRes = retRes + array[ix]*p
	end
	return retRes
end

pubTable.varianceValueForArray = function (array, minlim, maxlim, accuracy)
	local retRes, p = 0, accuracy/(maxlim-minlim)
	local expectValue = pubTable.expectValueForArray(array, p)
	for ix = 1, #array, 1 do
		retRes = retRes + math.pow(array[ix]-expectValue,2)*p
	end
	return retRes
end

------------------------------------------------------------------------
pubTable.expectValue = function (minlim, maxlim, accuracy)
	local retRes, p = 0, accuracy/(maxlim-minlim)
	for ix = minlim, maxlim, accuracy do
		retRes = retRes + accuracy*p
	end
	return retRes
end

pubTable.varianceValue = function (mu, minlim, maxlim, accuracy)
	local retRes, p = 0, accuracy/(maxlim-minlim)
	mu = mu or pubTable.expectValue(minlim, maxlim, accuracy)
	for ix = minlim, maxlim, accuracy do
		retRes = retRes + math.pow(ix-mu,2)*p
	end
	return retRes
end

pubTable.factorial = function (x)
	if x > 0 then
		retValue = x*pubTable.factorial(x-1)
	elseif x == 0 then
		retValue = 1
	end
	return retValue
end
------------------------------------------------------------------------
pubTable.gammaFunction = function (z, kmin)
	kmin = kmin or 0
	local retValue, kSum = 1, 0
	if kmin > 0 and z>0 then
		for k=0, z-1, 1 do
			kSum = kSum + math.pow(kmin, k)/pubTable.factorial(k)
		end
		retValue = pubTable.factorial(z-1)*math.exp(-kmin)*kSum
	elseif z > 0 then
		retValue = pubTable.factorial(z-1)
	end
	return retValue
end

pubTable.gammaFunctionEler = function (z, accuracy)
	local accuracy = accuracy or math.pow(10,6)
	local retValue = 1
	for ix = 1, accuracy, 1 do
		retValue = retValue * (math.pow((ix+1)/ix, z)/((ix+z)/ix))
	end
	return retValue/z
end

pubTable.fibonachiGen = function(arr, a, b)
	local retValue = 0
	local newNum = #arr + 1
	if arr[newNum-a] >= arr[newNum-b] then
		retValue = arr[newNum-a] - arr[newNum-b]
	else
		retValue = arr[newNum-a] - arr[newNum-b] + 1
	end
	return retValue
end

pubTable.paretoRandomNum = function(x, shape, x0)
	x0 = x0 or 0.1
	shape = shape or 3
	scale = scale or 1
	local retValue = 0
	retValue =  x0*math.pow(x, -1/shape)
	return retValue
end
-- if scale == 1 - Exponential distribution
-- if scale >= 1 - Erlang distribution
-- if scale == n/2 & shape = 1/2 - Chi-squared distribution
pubTable.gammaRandomNum = function(x, shape, scale)
	shape = shape or 1
	scale = scale or 1
	mu = mu or 0
	local retValue = 0
	
	local S1, S2 = 1, 1 
	local randValue1, randValue2, randValue3 = 0, 0, 0
	
	while (S1+S2) > 1 do
		randValue1 = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue1)
		randValue2 = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue2)	
		
		S1 = math.pow(randValue1, 1/shape)
		S2 = math.pow(randValue2, 1/(1-shape))
	end
	
	retValue = (S1*math.log(x))/(scale*(S1+S2))
	
	return retValue
end

pubTable.expRandomNum = function(x, shape)
	shape = shape or 1
	local retValue = 0
	
	retValue = -math.log(x)/shape
	
	return retValue
end

pubTable.erlangRandomNum = function(shape, m)
	shape = shape or 1 
	m = m or 2
	local retValue = 1
	for ix = 1, m, 1 do
		local randValue = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randValue)
		retValue = retValue*randValue
	end
	retValue = -math.log(retValue)/shape
	
	if retValue > 10 or retValue < 10 then
		retValue = 1
	end
	return retValue
end

pubTable.betaRandomNum = function(x, shapeAlpha, shapeBeta, Ga, Gb, Gab)
	shapeAlpha = shapeAlpha or 2
	shapeBeta = shapeBeta or 2
	Ga = Ga or pubTable.gammaFunction(shapeAlpha)
	Gb = Gb or pubTable.gammaFunction(shapeBeta)
	Gab = Gab or pubTable.gammaFunction(shapeAlpha + shapeBeta)
	local retValue = 0
	
	if x>0 and x<1 then
		retValue = math.pow(1/shapeAlpha-(x*Ga*Gb)/Gab, shapeAlpha+shapeBeta-2)
	end
	
	return retValue
end
------------------------------------------------------------------------
------------------------Non-math function-------------------------------
------------------------------------------------------------------------
pubTable.createRandomArr = function (maxArrSize)
	local rand  = assert (io.open ('/dev/random', 'rb'))
	local retArr = {}
	local randstr = rand:read(maxArrSize)
	
	for ix = 1, randstr:len(), 1 do
		table.insert(retArr, string.byte(randstr, ix)/(0xFF))
	end
	
	rand:close()
	return retArr
end
-- "cpufr", 3, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000 
-- "temp", 2, "/sys/class/thermal/thermal_zone", "/temp", 1000
pubTable.readSysNumericData =  function(_dataName, _dataQuantity, _firstPathPart, _secondPathPart, _divider)
	local retStr = ""
	
	for ix = 0, _dataQuantity, 1 do
		local file = io.open (_firstPathPart .. ix .. _secondPathPart, r)
		
		retStr = retStr .. _dataName .. "[" .. ix .. "]=" .. (tonumber(file:read())/_divider) 
		if(ix < _dataQuantity) then
			retStr = retStr .. "&"
		end
		io.close(file)
	end
	return retStr
end

pubTable.readSysArray =  function(array, _dataQuantity, _firstPathPart, _secondPathPart, _divider)
	table.insert(array,_dataQuantity+1)
	for ix = 0, _dataQuantity, 1 do
		local file = io.open (_firstPathPart .. ix .. _secondPathPart, r)
		
		table.insert(array,tonumber(file:read())/_divider) 
		io.close(file)
	end
	return array
end

pubTable.createRandomMessage = function(_mesSize)
	local str = ''
	for ix=1, _mesSize, 1 do
		str = str .. string.format("%x", math.random(0,16))
	end
	return str
end

pubTable.sendUdpMessage = function(_host, _port, _message)
	--local host = socket.dns.toip(_host)
	local udp = assert(socket.udp())
	--assert(udp:setpeername(_host, _port))
	assert(udp:sendto(_message, _host, _port))
	--assert(ip, port)
	--udp:sendto(dgram, ip, port)
end

------------------------------------------------------------------------
---------------------------Program body---------------------------------
------------------------------------------------------------------------
pubTable.body = function(arg)
	-- For random number generation
	pubTable.randomArr = pubTable.createRandomArr(55)
	-- For beta distribution
	--local shapeA, shapeB = 2, 2
	--local Ga, Gb, Gab = pubTable.gammaFunction(shapeA), pubTable.gammaFunction(shapeB), pubTable.gammaFunction(shapeA + shapeB)
	
	--print("----------Gammas--------------")
	--print("Exp: ", gammaValue)
	--print("Xi: ", xi_gammaValue)
	--print("Er2: ", er2_gammaValue)
	--print("Er3: ", er3_gammaValue)
	--print("------------------------------")
	
	--local client = mqtt.client.create(pubTable.ip, pubTable.port)
	--client:connect(pubTable.name)
	
	local loopCount, timeCount = 0, 0
	local timeSend, timeGenNum, delayGen = {}, {}, {}
	local b_time = 0
	local timeMaxCount = 1000
	
	while loopCount < 1000 do 
		if timeCount <= timeMaxCount then
			b_time = socket.gettime()
		end
		message = string.format( "query?%s&%s",
			pubTable.readSysNumericData("temp", 2, "/sys/class/thermal/thermal_zone", "/temp", 1000),
			pubTable.readSysNumericData("cpufr", 3, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000 )
		)
		--client:publish(pubTable.topic, message)
		--client:handler()
		--local tempArray, cpufArray = {}, {}
		--pubTable.readSysArray (tempArray, 2, "/sys/class/thermal/thermal_zone", "/temp", 1000)
		--pubTable.readSysArray (cpufArray, 3, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000)
		
		--libnyociClient.sendCoapMessage("coap://127.0.0.1/", 
		--						3, "/sys/class/thermal/thermal_zone", "/temp", 
		--						4, "/sys/bus/cpu/devices/cpu", "/cpufreq/scaling_cur_freq", 1000)
		--libnyociClient.sendString("coap://127.0.0.1/", message)
		--coapSender.sendMessage("localhost", 5683, message)
		--coapSender2.sendMessage("localhost", 5683, message)
		pubTable.sendUdpMessage(pubTable.ip, pubTable.port, message)
		
		if timeCount <= timeMaxCount then
			table.insert(timeSend, socket.gettime() - b_time)
			print(timeSend[#timeSend])
			b_time = socket.gettime()
		end
		local randomNumber = pubTable.fibonachiGen(pubTable.randomArr, 55, 24)
		table.insert(pubTable.randomArr, randomNumber)
		
		if timeCount <= timeMaxCount then
			table.insert(timeGenNum, socket.gettime() - b_time)
			b_time = socket.gettime()
		end
		repeat
			local msdelay = assert(pubTable.paretoRandomNum(randomNumber, 1)/10) 
					--pubTable.expRandomNum(randomNumber, 1)
					--pubTable.gammaRandomNum(randomNumber, 1/2, 1/2)/10
					--pubTable.gammaRandomNum(randomNumber, 1, 2)
					--pubTable.gammaRandomNum(randomNumber, 1, 3)
					--pubTable.betaRandomNum (randomNumber, 2, 2)
			if msdelay == math.huge or msdelay ~= msdelay or msdelay > 10 then 
				break
			end
			if timeCount <= timeMaxCount then
				table.insert(delayGen, socket.gettime() - b_time)
				timeCount = timeCount + 1
			end
			--print(msdelay)
			
			socket.sleep(msdelay)
			loopCount = loopCount + 1
		until true
	end
	
	local resultFile = io.open("results.txt", "a")
	
	local meanSendT, meanGenNumT, meanDelayGen = 0, 0, 0
	
	local maxSendT, minSendT = 0, 1
	local maxGenNumT, minGenNumT = 0, 1
	local maxDelayGen, minDelayGen = 0, 1
	
	resultFile:write("TimeResults:\nTime to send\tTime to gen random number\tTime to gen distribution\n")
	
	for ix = 1, timeCount, 1 do
		resultFile:write(timeSend[ix] .. "\t" .. timeGenNum[ix] .. "\t" .. delayGen[ix] .. "\n")
		if maxSendT < timeSend[ix] then
			maxSendT = timeSend[ix]
		end
		if minSendT > timeSend[ix] then 
			minSendT = timeSend[ix]
		end
		if maxGenNumT < timeGenNum[ix] then
			maxGenNumT = timeGenNum[ix]
		end
		if minGenNumT > timeGenNum[ix] then 
			minGenNumT = timeGenNum[ix]
		end
		if maxDelayGen < delayGen[ix] then
			maxDelayGen = delayGen[ix]
		end
		if minDelayGen > delayGen[ix] then 
			minDelayGen = delayGen[ix]
		end
		meanSendT = meanSendT + timeSend[ix]
		meanGenNumT = meanGenNumT + timeGenNum[ix]
		meanDelayGen = meanDelayGen + delayGen[ix]
	end
	resultFile:write("Mean values:\n" .. meanSendT/timeCount .. "\t" .. meanGenNumT/timeCount .. "\t" .. meanDelayGen/timeCount .. "\n")
	
	local timeAccuracy = 1e-9--math.pow(10, -9)
	
	local SendTDeviation = math.pow(pubTable.varianceValueForArray(timeSend, minSendT, maxSendT, timeAccuracy), 0.5)
	local GenNumTDeviation = math.pow(pubTable.varianceValueForArray(timeGenNum, minGenNumT, maxGenNumT, timeAccuracy), 0.5)
	local DelayGenDeviation = math.pow(pubTable.varianceValueForArray(delayGen, minDelayGen, maxDelayGen, timeAccuracy), 0.5)
	
	resultFile:write("Deviation values:\n" .. SendTDeviation .. "\t" .. GenNumTDeviation .. "\t" .. DelayGenDeviation .. "\n")
	
	resultFile:close()
	
	--client:disconnect()
	--client:destroy()
end

pubTable.body(arg)
