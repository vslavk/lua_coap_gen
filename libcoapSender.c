#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <coap2/coap.h>

int resolve_address(const char *host, uint16_t port, coap_address_t *dst) 
{

	struct addrinfo *res, *ainfo;
	struct addrinfo hints;
	int error, len=-1;

	memset(&hints, 0, sizeof(hints));
	memset(dst, 0, sizeof(*dst));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_UNSPEC;

	char service[5];
	sprintf(service, "%d", port);
	
	error = getaddrinfo(host, service, &hints, &res);

	if (error != 0) 
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(error));
		return error;
	}

	for (ainfo = res; ainfo != NULL; ainfo = ainfo->ai_next) 
	{
		switch (ainfo->ai_family) 
		{
			case AF_INET6:
			case AF_INET:
				len = dst->size = ainfo->ai_addrlen;
				memcpy(&dst->addr.sin6, ainfo->ai_addr, dst->size);
				goto finish;
			default:
			;
		}
	}

	finish:
	freeaddrinfo(res);
	return len;
}

void onReciveMessage(const coap_endpoint_t *local_interface, const coap_address_t *remote, 
					 coap_pdu_t *sent, coap_pdu_t *received, const coap_tid_t id) 
{
	coap_show_pdu(LOG_INFO, received);
}

int sendMessage(const char* host, uint16_t port, const unsigned char* data)
{
	coap_context_t  *ctx = NULL;
	coap_session_t *session = NULL;
	coap_address_t dst;
	coap_pdu_t *pdu = NULL;
	int result = EXIT_FAILURE;;

	coap_startup();

  /* resolve destination address where server should be sent */
	if (resolve_address(host, port, &dst) < 0)//"coap.me", "5683", &dst) < 0) 
	{
		coap_log(LOG_CRIT, "failed to resolve address\n");
		goto finish;
	}

  /* create CoAP context and a client session */
	ctx = coap_new_context(NULL);

	if (!ctx || !(session = coap_new_client_session(ctx, NULL, &dst,
                                                  COAP_PROTO_UDP))) 
    {
		coap_log(LOG_EMERG, "cannot create client session\n");
		goto finish;
	}

  /* coap_register_response_handler(ctx, response_handler); */
	coap_register_response_handler(ctx, (coap_response_handler_t)onReciveMessage);
  /* construct CoAP message */
	pdu = coap_pdu_init(COAP_MESSAGE_CON,
                      COAP_REQUEST_GET,
                      0 /* message id */,
                      coap_session_max_pdu_size(session));
	if (!pdu) 
	{
		coap_log( LOG_EMERG, "cannot create PDU\n" );
		goto finish;
	}

  /* add a Uri-Path option */
	//unsigned char* h1 = "hello";
	uint16_t dataLength = sizeof(data)/sizeof(char);
	coap_add_option(pdu, COAP_OPTION_URI_PATH, dataLength, data);

  /* and send the PDU */
	coap_send(session, pdu);

	coap_run_once(ctx, 0);

	result = EXIT_SUCCESS;
	
	finish:

	coap_session_release(session);
	coap_free_context(ctx);
	coap_cleanup();

	return result;
}

/*int main(void) 
{
	return sendMessage("localhost", 5683, "lohpidr");
}*/
